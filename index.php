<?php
session_start();
$start = microtime(true);

/////////////////////////////// CONFIG ///////////////////////////////

$header = "Directory Listing";
$hote = "http://ddl.dd.fr";
$path = "./"; // répertoire à scanner
$plusdir = ""; // initilisation
$printDirectorySize = true; //augmente considérablement le temps de chargement
$printFileSize = true;
$Authentification = true;
$motdepasse = "mdp";
$extensionsInterdites = array("swp", "php", "htaccess", "html", "log", "sql", "meta", "ex");

/////////////////////////////// AUTHENTIFICATION ///////////////////////////////
if($Authentification)
{
	if(!isset($_SESSION['l']))
	{
		if($_SERVER['REQUEST_METHOD'] == "POST")
		{
			if($_POST['mdp'] == $motdepasse)
				$_SESSION['l'] = true;
			else
				die("Mot de passe incorrect"); 
		}
	}
	if(isset($_GET['deconnexion']))
	{
		session_destroy();
		echo "Déconnexion effectuée !";
		header('Location: /');
	}
}
else
{
	$_SESSION['l'] = true; 
}

/////////////////////////////// FONCTIONS ///////////////////////////////
function encoder($url) // assure la compatibilité des caractères spéciaux avec les urls 
{
	$url = str_replace("&", "%26", $url);
	$url = str_replace("+", "%2B", $url);
	$url = str_replace("ô", "%C3%B4", $url);
	return $url;
}


function human_filesize($bytes, $decimals = 2) {
    if($bytes > 0)
    {
    	$size = array('o','Ko','Mo','Go','To','Po','Eo','Zo','Yo');
    	$factor = floor((strlen($bytes) - 1) / 3);
    	return sprintf("%.{$decimals}f ", $bytes / pow(1024, $factor)) . @$size[$factor];
    }
    else
    	return 0;
}

function dirsize($dir)
{
  @$dh = opendir($dir);
  $size = 0;
  while ($file = @readdir($dh))
  {
    if ($file != "." and $file != "..") 
    {
      $path = $dir."/".$file;
      if (is_dir($path))
      {
        $size += dirsize($path); // recursive in sub-folders
      }
      elseif (is_file($path))
      {
        $size += filesize($path); // add file
      }
    }
  }
  @closedir($dh);
  return $size;
}

/////////////////////////////// LISTEUR ///////////////////////////////

if(isset($_SESSION['l']) && $_SESSION['l'])
{
	if(isset($_GET['dir']) && $_GET['dir'] != "")
	    $plusdir = $_GET['dir']."/";
	else if(isset($_GET['fichier']) && $_GET['fichier'] != "") // CAKEBOX LIKE
	{
		echo "<video controls src='".$hote.'/'.$_GET['fichier']."'></video>";
		die();
	}

	$dir = $path.$plusdir;
	?>

	<!DOCTYPE html>
	<html>
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs-3.3.7/jq-2.2.4/dt-1.10.13/kt-2.2.0/r-2.1.0/datatables.min.css"/> <style>
			.lienr {
				width: 30px;
			}
		</style>
	</head>
	<body>
	<?php
	if($Authentification) echo "<a style='float: right;' href='?deconnexion'>Se Déconnecter</a>";
	?>
	<center><h1><?php echo $header; ?></h1></center><br><br>

	<div class="col-xs-10 col-xs-offset-1">

	<?php 
		echo "<b>Chemin actuel : /".explode($path, $dir)[1]."</b><br><br>"; 
	?>

	<table id="fichiers" class="table table-striped table-bordered" cellspacing="0" width="100%">
	        <thead>
	            <tr>
	                <th>Nom du fichier</th>
	                <th>Taille</th>
	            </tr>
	        </thead>
	        <tbody>

	<?php
	foreach(scandir($dir) as $file)
	{
		if($file != ".." && $file != ".")
		{
			$directDIR = encoder($plusdir.$file);

		    if(is_dir($dir.$file))
		    {
		    	$size = 0;
		    	if($printDirectorySize)
			{	
				$brutsize = dirsize($dir.$file);
		    		$size = "<td data-order='".$brutsize."'>".human_filesize($brutsize)."</td>";			
			}
			else
				$size = "<td><b>Dossier</b></td>";
		    	echo "<tr>
		    			<td><b><a href=\"?dir=".$directDIR."\">".$file."</a></b></td>
		    			".$size."
				</tr>";
		    }
		    else
		    {
		    	if($printFileSize)
		    		$size = filesize($dir.$file);
		    	$ext = end(explode(".",$file));
		    	if(!in_array($ext, $extensionsInterdites)) // on n'affiche pas les extension interdites
		    	{
		  		echo "<tr>
		  				<td><a href=\"".$hote.'/'.$directDIR."\">".$file."</a></td>
		  				<td data-order='".$size."'>".human_filesize($size)."</td>
		  			</tr>";
		    	}
		    }
		}
	}
	?>

	        </tbody>
	    </table>

	<?php

	$urlmoins = explode($path, $dir)[1];
	$urls = explode('/', $urlmoins);

	$urlretour = "";
	if(count($urls) > 1)
	{
		for ($i=0; $i < count($urls)-2; $i++) // jusqu'a l'avant dernière
		{ 
			if($urls[$i] != "")
			{	if($i == 0)
		    		$urlretour .= "".$urls[$i];
		    	else
		    		$urlretour .= "/".$urls[$i];
			}
		}
		if($urlretour != "")
			echo "<a class='btn btn-primary' href='?dir=".encoder($urlretour)."'>Retour</a>";
		else
			echo "<a class='btn btn-primary' href='/'>Retour</a>";
		echo "<br>";
	}

	$end = microtime(true);
	$creationtime = ($end - $start);
	printf("Page générée en %.2f ms (PHP) + <span id='execJS'></span> ms (JavaScript)", $creationtime*1000);

	?>

	</div>
	<script type="text/javascript" src="https://cdn.datatables.net/v/bs-3.3.7/jq-2.2.4/dt-1.10.13/kt-2.2.0/r-2.1.0/datatables.min.js"></script>

	<script type="text/javascript">
	Debut = new Date();
	//$(document).ready(function() {
	    $('#fichiers').DataTable({
	    	"aoColumnDefs": [
		          { 'bSortable': true, 'aTargets': [ 1 ] }
		       ],
	        "language": {
				"sProcessing":     "Traitement en cours...",
				"sSearch":         "Rechercher&nbsp;:",
			    "sLengthMenu":     "Afficher _MENU_ fichiers",
				"sInfo":           "_START_ - _END_ [_TOTAL_]",
				"sInfoEmpty":      "Aucun fichier à affichier",
				"sInfoFiltered":   "(filtré de _MAX_ fichiers au total)",
				"sInfoPostFix":    "",
				"sLoadingRecords": "Chargement en cours...",
			    "sZeroRecords":    "Aucun fichier dans ce dossier",
				"sEmptyTable":     "Aucun fichier dans ce dossier",
				"oPaginate": {
					"sFirst":      "Premier",
					"sPrevious":   "Précédent",
					"sNext":       "Suivant",
					"sLast":       "Dernier"
				},
				"oAria": {
					"sSortAscending":  ": activer pour trier la colonne par ordre croissant",
					"sSortDescending": ": activer pour trier la colonne par ordre décroissant"
				},
			},
			"lengthMenu": [[10, 15, 20, 40, 50, 100, 200, -1], [10, 15, 20, 40, 50, 100, 200, "Tout"]],
	        "pageLength": 15
	    } );
	//} );

	Fin = new Date();
	$("#execJS").html((Fin-Debut));

	</script>

	</body>
	</html>

<?php
}
else
{?>
	<!DOCTYPE html>
	<html>
	<head>
		<meta charset="UTF-8">
		<title>Login</title>
		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs-3.3.7/jq-2.2.4/dt-1.10.13/kt-2.2.0/r-2.1.0/datatables.min.css"/>
	</head>
	<body>
		<div class="col-xs-4 col-xs-offset-4">
			<h1>Accès Restreint !</h1><br><br>
			<?php
				if (isset($_GET['dir']))
					$dir_pp = "?dir=".$_GET['dir'];
				else
					$dir_pp = "";
			?>
			<form action=".<?php echo $dir_pp; ?>" method="POST">
				<label>Mot de passe : </label><input class="form-control" type="password" name="mdp"><br>
				<input class="btn btn-primary btn-lg" type="submit" value="Connexion">
			</form>
		<br><br>

		</div>

	</body>
	</html>

	<style>
	body {
		text-align: center;
	}
	</style>

<?php	
}
?>
